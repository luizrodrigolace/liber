<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CommentController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('books',[BookController::class,'index']);

Route::get('books/{id}',[BookController::class,'show']);

Route::post('books',[BookController::class,'create']);

Route::put('books/{id}',[BookController::class,'update']);

Route::delete('books/{id}',[BookController::class,'destroy']);


Route::get('customers',[CustomerController::class,'index']);

Route::get('customers/{id}',[CustomerController::class,'show']);

Route::post('customers',[CustomerController::class,'create']);

Route::put('customers/{id}',[CustomerController::class,'update']);

Route::delete('customers/{id}',[CustomerController::class,'destroy']);


Route::get('comments',[CommentController::class,'index']);

Route::get('comments/{id}',[CommentController::class,'show']);

Route::post('comments',[CommentController::class,'create']);

Route::put('comments/{id}',[CommentController::class,'update']);

Route::delete('comments/{id}',[CommentController::class,'destroy']);