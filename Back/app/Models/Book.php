<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Book extends Model
{
    public function createBook(Request $request){
        $this->title = $request->title;
        $this->genre = $request->genre;
        $this->price = $request->price;
        $this->author = $request->author;
        $this->condition = $request->condition;
        $this->rating = $request->rating;
        $this->save();
    }

    public function customers(){
        return $this->belongsTo('App\Customers');
    }
    public function comments(){
        return $this->hasMany('App\Comments');
    }
    
}
