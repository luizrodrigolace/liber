<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    public function createComment(Request $request){
        $this->text = $request->text;
        $this->save();
    }
    public function customers(){
        return $this->belongsTo('App\Customers');
    }
    public function books(){
        return $this->belongsTo('App\Books');
    }
}
