<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function createCustomer(Request $request){
        $this->name = $request->name;
        $this->email = $request->email;
        $this->adress = $request->adress;
        $this->phone_number = $request->phone_number;
        $this->birth_date = $request->birth_date;
        $this->gender = $request->gender;
        $this->save();
    }

    public function books(){
        return $this->hasMany('App\Books');
    }
    public function comments(){
        return $this->hasMany('App\Comments');
    }
}
