<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Comment;

class CommentController extends Controller
{
    public function create(Request $request){
        $comment = new Comment;
        $comment->text = $request->text;
        $comment->customer_id =$request->customer_id;
        $comment->book_id =$request->book_id;
        $comment->save();
        return response()->json(['comment' => $comment],200);
    }
    public function index(){
        $comment = Comment::all();
        return response()->json(['comment' => $comment],200);

    }

    public function show($id){
        $comment = Comment::findOrFail($id);
        return response()->json(['comment' => $comment],200);
    }

    public function update(Request $request, $id){
        $comment = Comment::find($id);
        if($request->text){
            $comment->text = $request->text;
        }
        if($request->text){
            $comment->book_id = $request->book_id;
        }
        if($request->text){
            $comment->customer_id = $request->customer_id;
        }

        $comment->save();
        return response()->json(['comment' => $comment],200);
    }

    public function destroy($id){
        $comment = Comment::find($id);
        $comment->delete();
        return response()->json(['Comentario deletado com sucesso!'],200);
    }

}
