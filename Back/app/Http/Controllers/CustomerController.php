<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerController extends Controller
{
    public function create(Request $request){
        $customer = new Customer;
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->adress = $request->adress;
        $customer->phone_number = $request->phone_number;
        $customer->birth_date = $request->birth_date;
        $customer->gender = $request->gender;
        $customer->save();
        return response()->json(['customer' => $customer],200);

    }
    public function index(){
        $customers = Customer::all();
        return response()->json(['customer' => $customers],200);

    }

    public function show($id){
        $customer = Customer::findOrFail($id);
        return response()->json(['customer' => $customer],200);
    }

    public function update(Request $request, $id){
        $customer = Customer::find($id);
        if($request->name){
            $customer->name = $request->name;
        }
        if($request->email){
            $customer->email = $request->email;
        }
        if($request->adress){
            $customer->adress = $request->adress;
        }
        if($request->phone_number){
            $customer->phone_number = $request->phone_number;
        }
        if($request->birth_date){
            $customer->birth_date = $request->birth_date;
        }
        if($request->gender){
            $customer->gender = $request->gender;
        }

        $customer->save();
        return response()->json(['customer' => $customer],200);
    }

    public function destroy($id){
        $customer = Customer::find($id);
        $customer->delete();
        return response()->json(['Cliente deletado com sucesso!'],200);
    }
}


